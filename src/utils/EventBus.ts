import mitt from 'mitt';

// 使用 Event Bus

const EventBus = mitt();

export default EventBus;

import request from '@/utils/request';
import {ListenerFieldForm, ListenerForm, ListenerQuery, ListenerVO} from './types';
import {AxiosPromise} from 'axios';

// 分页查询流程监听器
export function queryListenerPage(query?: ListenerQuery): AxiosPromise<ListenerVO[]> {
  return request({
    url: '/workflow/listener/queryPage',
    method: 'get',
    params: query
  });
}

// 列表查询流程监听器
export function queryListenerList(query?: ListenerQuery): AxiosPromise<ListenerVO[]> {
  return request({
    url: '/workflow/listener/queryList',
    method: 'get',
    params: query
  });
}

// 详细查询流程监听器
export function getListener(formId: string | number): AxiosPromise<ListenerVO> {
  return request({
    url: '/workflow/listener/query/' + formId,
    method: 'get'
  });
}

// 新增流程监听器
export function addListener(data: ListenerForm) {
  return request({
    url: '/workflow/listener/insert',
    method: 'post',
    data: data
  });
}

// 修改流程监听器
export function updateListener(data: ListenerForm) {
  return request({
    url: '/workflow/listener/update',
    method: 'post',
    data: data
  });
}

// 删除流程监听器
export function delListener(listenerId?: string | number | (string | number)[]) {
  return request({
    url: '/workflow/listener/delete/' + listenerId,
    method: 'post'
  });
}

// 新增流程监听器字段
export function insertListenerFieldAPI(data: ListenerFieldForm) {
  return request({
    url: '/workflow/listener/insertField',
    method: 'post',
    data: data
  });
}

// 修改流程监听器字段
export function updateListenerFieldAPI(data: ListenerFieldForm) {
  return request({
    url: '/workflow/listener/updateField',
    method: 'post',
    data: data
  });
}

// 删除流程监听器字段
export function deleteListenerFieldAPI(fieldIds?: string | number | (string | number)[]) {
  return request({
    url: '/workflow/listener/deleteField/' + fieldIds,
    method: 'post'
  });
}

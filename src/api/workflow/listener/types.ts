export interface ListenerVO {
  id: number | string;
  listenerType: string;
  name: string;
  eventType: string;
  valueType: string;
  value: string;
  fields: ListenerFieldVO[];
}

export interface ListenerForm {
  id: number | string | undefined;
  listenerType: string;
  name: string;
  eventType: string;
  valueType: string;
  value: string;
}

export interface ListenerQuery extends PageQuery {
  listenerType?: string;
  eventType?: string;
  name?: string;
}

export interface ListenerFieldVO {
  id: number | string;
  listenerId: number | string;
  fieldName: string;
  fieldType: string;
  fieldValue: string;
}

export interface ListenerFieldForm {
  id: number | string | undefined;
  listenerId: number | string | undefined;
  fieldName: string | undefined;
  fieldType: string | undefined;
  fieldValue: string | undefined;
}
